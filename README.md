# H7_CoolProp

step compile cmake

#make

cmake . -G "MinGW Makefiles" -DCOOLPROP_STATIC_LIBRARY=ON -DCMAKE_DL_LIBS==OFF -DCMAKE_BUILD_TYPE=Release

#tambahkan di flags.make

CXX_FLAGS = -O3 -DNDEBUG  -mfloat-abi=hard -mcpu=cortex-m7

#build

cmake --build . -- -j 4
